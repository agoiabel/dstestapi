const jwt = require('jsonwebtoken');
const SECRET = 'DSTESTAPI' //this should be removed from production

module.exports = {
	issuer(payload) {
		return jwt.sign(payload, SECRET);
	},
	verify(token) {
		return jwt.verify(token, SECRET);
	}
}