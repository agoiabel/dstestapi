module.exports.getProduct = async function (productId) {
		return await Product.findOne({id: productId});
}

module.exports.storeCartItemFrom = async function (cart, product, cart_item) {
      	const params = {
        	cart_id: cart.id,
        	product_id: product.id,
        	quantity: cart_item.quantity	
    	}
        return await Cart_item.create(params);
}

module.exports.storeTransaction = async function (transactionAmount, user_id, cart_id) {
  	const params = {
    	reference: Date.now(),
    	amount: transactionAmount,
    	user_id: user_id,
    	cart_id: cart_id
    }

    return await Transaction.create(params).fetch();
}
