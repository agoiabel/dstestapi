const bcrypt = require('bcrypt');
const SALT_ROUND = 10;

module.exports = {
    async hash (password) {
        return await bcrypt.hash(password, SALT_ROUND);
    },

    async compare (password, hashed) {
        return await bcrypt.compare(password, hashed);
    }
}