const encrypt = require('../utitlities/encryt');

/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    firstname: {
      type: "string",
      required: true
    },
    lastname: {
      type: "string",
      required: true
    },
    email: {
      type: "string",
      required: true,
      unique: true,
      isEmail: true
    },
    password: {
      type: "string",
      required: true,
      maxLength: 200
    },
    phonenumber: {
      type: "string",
      required: true,
      minLength: 10,
      maxLength: 11
    },

    //Associate
    carts: {
        collection: 'cart',
        via: 'user_id'
    }

  },
};

