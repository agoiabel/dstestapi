/**
 * Product.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    name: {
        type: 'string',
        required: true
    },
    description: {
        type: 'string',
        required: true
    },
    amount: {
        type: 'number',
        required: true
    },
    image_path: {
        type: 'string',
        required: true
    },

    //Associate
    cart_items: {
        collection: 'cart_item',
        via: 'product_id'
    }

  },

};

