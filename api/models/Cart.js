/**
 * Cart.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
        type: 'string',
        required: true
    },
    shipping_address: {
        type: 'string',
        required: true
    },
    user_id: {
        model: 'user'
    },
    
    cart_items: {
        collection: 'cart_item',
        via: 'cart_id'
    }

  },

};