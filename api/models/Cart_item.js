/**
 * Cart_item.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    quantity: {
        type: 'number',
        required: true  
    },
    cart_id: {
        model: 'cart'
    },
    product_id: {
        model: 'product'
    },
  },

};

