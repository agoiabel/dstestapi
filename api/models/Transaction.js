/**
 * Transaction.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    cart_id: {
        model: 'cart'
    },
    user_id: {
        model: 'user'
    },
    amount: {
        type: 'number',
        required: true
    },
    paid: {
        type: "boolean",
        defaultsTo: false
    },
    reference: {
        type: 'string',
        required: true        
    }
  },

};

