const Joi = require('joi');

const rules = {
    store: Joi.object().keys({
        image_path: Joi.string().min(3).required(),
        product_id: Joi.string().required(),
    })
}

module.exports = rules;