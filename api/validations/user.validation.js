const Joi = require('joi');

const rules = {
    store: Joi.object().keys({
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        firstname: Joi.string().min(3).max(30).required(),
        lastname: Joi.string().min(3).max(30).required(),
        phonenumber: Joi.number().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
    }),

	authentication: Joi.object().keys({
        email: Joi.string().email({ minDomainAtoms: 2 }).required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
    })
}

module.exports = rules;