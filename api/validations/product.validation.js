const Joi = require('joi');

const rules = {
    store: Joi.object().keys({
        name: Joi.string().min(3).max(30).required(),
        description: Joi.string().min(3).max(200).required(),
        image_path: Joi.string().min(3).max(200).required(),
        amount: Joi.number().required(),
    })
}

module.exports = rules;