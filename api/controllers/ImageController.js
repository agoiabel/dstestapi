/**
 * ImageController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const Joi = require('joi');
const validate = require('../validations/image.validation');

module.exports = {
  store: async function (req, res) {
        //validate req.body
        const { error } = Joi.validate(req.body, validate.store);
        if (error) {
            return res.status(400).send(error);
        }

        //save new user
        try {
            const image = await Image.create({
                image_path: req.body.image_path,
                product_id: req.body.product_id
            }).fetch();

            return res.status(200).send({
                image: image,
                status: 200
            });
        } catch(err) {
            return res.serverError(err);
        }

  }
};

