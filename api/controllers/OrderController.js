/**
 * OrderController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const {getProduct, storeCartItemFrom, storeTransaction} = require('../utitlities/Product.js');
// const paystack = require('paystack')('sk_test_da5d641bf006413a188c2b383c4b263823e195a5');
const request = require('request');
const Nexmo = require('nexmo')

module.exports = {
	store: async function (req, res) {

		//Find the user
        try {
            //get user
            const user = await User.findOne({
                email: req.body.user_email
            });

            //get cart
            const cart = await Cart.create({
            	name: Date.now(),
            	shipping_address: req.body.shipping_address.address +' '+ req.body.shipping_address.state,
            	user_id: user.id 
            }).fetch();


            var transactionAmount = 0;
            const cart_items = req.body.cart_items;
            
            for (var j = 0; j < cart_items.length; j++) {
	           	const product = await getProduct(cart_items[j].product.id);

            	transactionAmount += product.amount * cart_items[j].quantity;

            	await storeCartItemFrom(cart, product, cart_items[j]);
            }

            let transaction = await storeTransaction(transactionAmount, user.id, cart.id);
            transaction['user'] = {
            	firstname: user.firstname,
            	lastname: user.lastname,
            	phonenumber: user.phonenumber,
            	email: user.email
            }

            return res.json({
                data: {
                    transaction: transaction,
                },
                status: 200,
                message: 'Product was initiated successfully'
            });

        } catch (error) {
            return res.json({
                data: {
                    transaction: ''
                },
                status: 422,
                message: 'Something went wrong'
            });
        }

	},

	confirm: async function (req, res) {

        const transaction = await Transaction.findOne({
            reference: req.body.reference
        });
        const user = await User.findOne({
            id: transaction.user_id
        });


		var options = {
		  url: `https://api.paystack.co/transaction/verify/${req.body.reference}`,
		  headers: {
		  	'Content-Type': 'application/json',
		    'Authorization': 'Bearer sk_test_da5d641bf006413a188c2b383c4b263823e195a5'
		  }
		};
	
		request(options, function callback(error, response, body) {
		  if (!error && response.statusCode == 200) {
			    const result = JSON.parse(body);

			    //should check amount too, but ......not time...lolz
			    if (result.status) {
			    	//send email
                    sails.hooks.email.send(
                         "sendEmail",
                         {
                             Name: "Email",
                         },
                         {
                             to: `${user.email}`,
                             subject: "Purchased was successful-DSTest"
                         }, function(err) {
                            console.log(err || "Mail Sent!");
                        }
                     )


                    //send sms
                    const nexmo = new Nexmo({
                      apiKey: '3aa22f54',
                      apiSecret: '17885e0d219a589d'
                    });

                    const from = 'Nexmo'
                    const to = `${user.phonenumber}`
                    const text = 'Product was purchased'

                    nexmo.message.sendSms(from, to, text);


                    res.send(result);
 			    }

		  }
		});
	}



};

