/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const Joi = require('joi');
const _ = require('lodash');
const encrypt = require('../utitlities/encryt');
const jwt = require('../utitlities/jwt.service');
const validate = require('../validations/user.validation');

module.exports = {
    store: async function (req, res) {

        //validate req.body
        const { error } = Joi.validate(req.body, validate.store);
        if (error) {
            return res.status(400).send(error);
        }

        //save new user
        try {
            const user = await User.create({
                email: req.body.email,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                phonenumber: req.body.phonenumber,
                password: await encrypt.hash(req.body.password)
            }).fetch();

            return res.json({
                data: {
                    user: user
                },
                status: 200,
                message: `Registration was successful, you can now login [${Date.now()}]`
            });
        } catch(err) {
            return res.json({
                data: {
                    user: ''
                },
                status: 422,
                message: `Registration was unsuccessful, [${Date.now()}]`
            });
        }
    },

    authenticate: async function (req, res) {
        
        //validate req.body
        const { error } = Joi.validate(req.body, validate.authentication);
        if (error) {
            return res.status(400).send(error);
        }

        try {
            //validate user
            const user = await User.findOne({
                email: req.body.email
            });

            if (! await encrypt.compare(req.body.password, user.password)) {
                return res.json({
                    data: {
                        user: '',
                        token: ''
                    },
                    status: 422,
                    message: `Password not valid [${Date.now()}]`
                });
            }

            //TODO::Move res.json to a single function that others will reference
            return res.json({
                data: {
                    user: _.pick(user, [
                        'email',
                        'firstname',
                        'lastname',
                        'phonenumber'
                    ]),
                    products: await Product.find(),
                    token: jwt.issuer({userId: user.id})
                },
                status: 200,
                message: 'Authentication was successful'
            });

        } catch (err) {
            return res.json({
                data: {
                    user: '',
                    token: ''
                },
                status: 422,
                message: `Email is not valid [${Date.now()}]`
            });
        }
    }
    
};

