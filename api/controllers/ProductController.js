/**
 * ProductController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const Joi = require('joi');
const validate = require('../validations/product.validation');

module.exports = {
  store: async function (req, res) {
        //validate req.body
        const { error } = Joi.validate(req.body, validate.store);
        if (error) {
            return res.status(400).send(error);
        }

        //save new user
        try {
            const product = await Product.create({
                name: req.body.name,
                amount: req.body.amount,
                image_path: req.body.image_path,
                description: req.body.description,
            }).fetch();

            return res.status(200).send({
                product: product,
                status: 200
            });
        } catch(err) {
            return res.serverError(err);
        }
  },

  show: async function (req, res) {

    try {
        const product = await Product.findOne({
            id: req.param('id')
        });

        return res.json({
            data: {
                product: product
            },
            status: 200,
            message: 'Product gotten successful'
        });

    } catch (err) {
        return res.json({
            data: {
                product: null,
            },
            status: 422,
            message: `Product does not exists [${Date.now()}]`
        });
    }
  }

};

